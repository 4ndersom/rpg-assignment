package aom.RPG;

public class Mage extends Character{

    private Attributes baseAttributes = new Attributes(1, 1, 8);
    private Attributes attributeIncrease = new Attributes(1, 1, 5);

    public Mage(String name){
        super(name);
        // Attributes in order Str, Dex, Int

        setBaseAttributes(baseAttributes);
        setTotalAttributes(baseAttributes);
        setAttributeIncrease(attributeIncrease);

    }
}
