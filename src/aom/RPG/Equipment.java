package aom.RPG;

public abstract class Equipment {
    // Fields/State
    private String name;
    private int requiredLevel;

    // Constructor
    public Equipment(String name, int requiredLevel){
        this.name = name;
        this.requiredLevel = requiredLevel;

    }

    //Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

}
