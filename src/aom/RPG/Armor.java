package aom.RPG;

public class Armor extends Equipment{
    private ArmorType armorType;
    private ArmorSlot armorSlot;
    private Integer[] equipmentBoost;


    //Constructor
    public Armor(String name, int requiredLevel, ArmorSlot armorSlot, ArmorType armorType, int strBoost, int dexBoost, int intBoost){
        super(name, requiredLevel);
        setArmorType((armorType));
        setArmorSlot((armorSlot));
        this.equipmentBoost = new Integer[3];
        this.equipmentBoost[0] = strBoost;
        this.equipmentBoost[1] = dexBoost;
        this.equipmentBoost[2] = intBoost;
    }

    //Setters and Getters

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public ArmorSlot getArmorSlot() {
        return armorSlot;
    }

    public void setArmorSlot(ArmorSlot armorSlot) {
        this.armorSlot = armorSlot;
    }

    public Integer[] getEquipmentBoost() {
        return equipmentBoost;
    }

    public void setEquipmentBoost(Integer[] equipmentBoost) {
        this.equipmentBoost = equipmentBoost;
    }
}
