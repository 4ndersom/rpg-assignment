package aom.RPG;

public class Weapon extends Equipment{
    private WeaponType weaponType;
    private int weaponDamage;
    private int weaponAttacksPerSecond;
    private Integer weaponDPS;

    public Weapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage, int weaponAttacksPerSecond){
        super(name, requiredLevel);
        setWeaponType((weaponType));
        setWeaponDamage((weaponDamage));
        setWeaponAttacksPerSecond((weaponAttacksPerSecond));
        setWeaponDPS((weaponDamage*weaponAttacksPerSecond));
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }

    public void setWeaponDamage(int weaponDamage) {
        this.weaponDamage = weaponDamage;
    }

    public int getWeaponAttacksPerSecond() {
        return weaponAttacksPerSecond;
    }

    public void setWeaponAttacksPerSecond(int weaponAttacksPerSecond) {
        this.weaponAttacksPerSecond = weaponAttacksPerSecond;
    }

    public Integer getWeaponDPS() {
        return weaponDPS;
    }

    public void setWeaponDPS(Integer weaponDPS) {
        this.weaponDPS = weaponDPS;
    }
}