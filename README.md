# RPG Assignment

This Java program is supposed to be an RPG console game, but needs further work to function properly.

## Installation

To run it, clone the repository and open the project in IntelliJ (will likely also work in other Java IDE's, but no guarantees are given – also it is still not complete anyway).

## Usage

```
Functional features:
You can create a mage character by instantiating it as an object of the Mage class using the new key word and passing a custom name as parameter, eg.
Mage name = new Mage(”name”);

The Mage can be levelled up by calling
name.levelUp();
This will update the mage's attributes (strength, dexterity, intelligence)

Armor can be created as instance of the Armor class:
Parameters:
name of the armor
required level to equip armor
equipment slot where the piece of armor is worn (as the enumtype ArmorSlot (HEAD, BODY, LEGS)
armor type (as enumtype ArmorType (CLOTH, LEATHER, MAIL or PLATE)
boost to strength
boost to dexterity
boost to intelligence
Example;
Armor tinFoilHat = new Armor("Tin Foil Hat", 1, ArmorSlot.HEAD, ArmorType.PLATE, 1, 1, 2)

Weapons can be created as instances of the Weapon class:
Parameters:
name of weapon
required level to equip weapon
weapon type (as the enumtype WeaponType (WAND, STAFF, BOW, HAMMER, AXE, SWORD, DAGGER)
weaponDamage (per attack)
(weapon) attacks per second
Example:
Weapon swordOfAThousandTruths = new Weapon("Sword of a Thousand Truths", 2, WeaponType.SWORD, 120, 1);


Features intended to be made given more time:
A method for equipping weapons and armor with a method for checking whether the characters level suffices and if the character can wear that type of armor or weapon.
Also providing these checks with custom exception handling.

Logic for automatically integrating base attributes with boosts form equipment and weapon damage to calculate and update the damage per second (DPS) that the character can deal.

A method (using a stringbuilder) for displaying the characters stats (base attributes, equipment boost and total attributes along with total DPS)

Unit tests for all methods.

A Warrior- Rogue- and Ranger class extending the abstract character class (logic could be copy pasted from the Mage class).
```

## Contributing
Developer and maintainer: Anders O. Madsen \
Thanks to Jakob Henriksen for help with the attributes class and to Helle Bertelsen for assistance with enums.
