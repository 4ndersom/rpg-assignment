package aom.RPG;

public class Attributes {       //Credits: Jakob Henriksen
    private Integer strength;
    private Integer dexterity;
    private Integer intelligence;


    public Attributes(Integer strength, Integer dexterity, Integer intelligence) {
        setStrength((strength));
        setDexterity((dexterity));
        setIntelligence((intelligence));

    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getDexterity() {
        return dexterity;
    }

    public void setDexterity(Integer dexterity) {
        this.dexterity = dexterity;
    }

    public Integer getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(Integer intelligence) {
        this.intelligence = intelligence;
    }
}
