package aom.RPG;

import java.util.HashMap;

public abstract class Character {
    // Fields/State
    private String name;
    private Integer level;
    private Attributes baseAttributes;
    private Attributes equipmentBoost;
    private Attributes totalAttributes;
    private HashMap<String, String> slots; // for equipment
    private StringBuilder stats; //for displaying attributes
    private Attributes attributeIncrease; // for increasing attr.s on lvl up

    // Constructor
    public Character(String name) {
        setName((name));
        setLevel((1));
        setBaseAttributes((baseAttributes));
        setEquipmentBoost((equipmentBoost));
        setTotalAttributes((totalAttributes));
        /*this.slots.put("Head", "beanie");
        this.slots.put("Body", "semi fancy jacket");
        this.slots.put("Legs", "outdoor pants");
        this.slots.put("Weapon", "Bare fists");*/
        setSlots((slots));
        setStats((stats)); // FIX

    }

    // Methods/Behaviors
    /*public void updateAttributes() {
        totalAttributes[0] = baseAttributes[0] + equipmentBoost[0];
        totalAttributes[1] = baseAttributes[1] + equipmentBoost[1];
        totalAttributes[2] = baseAttributes[2] + equipmentBoost[2];*/

    //}

    public void levelUp() {
        level += 1;
        baseAttributes.setStrength(attributeIncrease.getStrength());
        baseAttributes.setDexterity(attributeIncrease.getDexterity());
        baseAttributes.setIntelligence(attributeIncrease.getIntelligence());
        //updateAttributes();

    }

    public void displayStats() {
        System.out.println(stats);
    }

// Getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Attributes getBaseAttributes() {
        return baseAttributes;
    }

    public void setBaseAttributes(Attributes baseAttributes) {
        this.baseAttributes = baseAttributes;
    }

    public Attributes getEquipmentBoost() {
        return equipmentBoost;
    }

    public void setEquipmentBoost(Attributes equipmentBoost) {
        this.equipmentBoost = equipmentBoost;
    }

    public Attributes getTotalAttributes() {
        return totalAttributes;
    }

    public void setTotalAttributes(Attributes totalAttributes) {
        this.totalAttributes = totalAttributes;
    }

    public HashMap<String, String> getSlots() {
        return slots;
    }

    public void setSlots(HashMap<String, String> slots) {
        this.slots = slots;
    }

    public StringBuilder getStats() {
        // Stringbuilder stats here
        return stats;
    }

    public void setStats(StringBuilder stats) {
        this.stats = stats;
    }

    public Attributes getAttributeIncrease() {
        return attributeIncrease;
    }

    public void setAttributeIncrease(Attributes attributeIncrease) {
        this.attributeIncrease = attributeIncrease;
    }
}